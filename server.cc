#include "Bug.hh"
#include <iostream>
#include <cstdlib>

#ifndef OMNI_PORT
#define OMNI_PORT="11111"
#endif

static CORBA::Boolean bindObjectToName(CORBA::ORB_ptr, CORBA::Object_ptr);

namespace Bug
{
    class Demo_impl:public POA_Bug::Demo
    {
    public:
        Demo_impl() { }
        virtual ~Demo_impl() {}
        void call(Bug::OptionalValue *value);
    };

    void Demo_impl::call(Bug::OptionalValue *value)
    {
        if (value == NULL) {
            throw Bug::Demo::WRONG_DATA(NULL);
        }
        switch (value->_boxed_in()) {
            case Bug::MISSING_VALUE:
                throw Bug::Demo::WRONG_DATA(NULL);
            case Bug::BAD_VALUE:
                throw Bug::Demo::WRONG_DATA(new Bug::OptionalValue(Bug::BAD_VALUE));
            case Bug::CORRECT_VALUE:
                return;
        }
        throw Bug::Demo::WRONG_DATA(new Bug::OptionalValue(Bug::BAD_VALUE));
    }
}

//////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
    try {
        CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);
        CORBA::Object_var obj = orb->resolve_initial_references("RootPOA");
        PortableServer::POA_var poa = PortableServer::POA::_narrow(obj);
        PortableServer::Servant_var< Bug::Demo_impl > mytest = new Bug::Demo_impl();
        PortableServer::ObjectId_var mytestid = poa->activate_object(mytest);
        // Obtain a reference to the object, and register it in
        // the naming service.
        obj = mytest->_this();
        CORBA::String_var sior(orb->object_to_string(obj));
        std::cout << sior << std::endl;
        if (!bindObjectToName(orb, obj)) {
            return EXIT_FAILURE;
        }
        PortableServer::POAManager_var pman = poa->the_POAManager();
        pman->activate();
        orb->run();
    }
    catch (const CORBA::SystemException &e) {
        std::cerr << "Caught CORBA::" << e._name() << std::endl;
        std::cout << argv[0] << " done" << std::endl;
        return EXIT_FAILURE;
    }
    catch (const CORBA::Exception &e) {
        std::cerr << "Caught CORBA::Exception: " << e._name() << std::endl;
        std::cout << argv[0] << " done" << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << argv[0] << " done" << std::endl;
    return EXIT_SUCCESS;
}
//////////////////////////////////////////////////////////////////////

CORBA::Boolean
bindObjectToName(CORBA::ORB_ptr orb, CORBA::Object_ptr objref)
{
    CosNaming::NamingContext_var rootContext;
    try {
        // Obtain a reference to the root context of the Name service:
//        CORBA::Object_var obj = orb->resolve_initial_references("NameService");
        CORBA::Object_var obj = orb->string_to_object("corbaname::localhost:" OMNI_PORT);
        // Narrow the reference returned.
        rootContext = CosNaming::NamingContext::_narrow(obj);
        if (CORBA::is_nil(rootContext)) {
            std::cerr << "Failed to narrow the root naming context." << std::endl;
            return false;
        }
    }
    catch (const CORBA::NO_RESOURCES&) {
        std::cerr << "Caught NO_RESOURCES exception. You must configure omniORB "
                     "with the location" << std::endl
                  << "of the naming service." << std::endl;
        return false;
    }
    catch (const CORBA::ORB::InvalidName&) {
        // This should not happen!
        std::cerr << "Service required is invalid [does not exist]." << std::endl;
        return false;
    }
    try {
        // Bind a context called "test" to the root context:
        CosNaming::Name contextName;
        contextName.length(1);
        contextName[0].id   = "my_context"; // string copied
        contextName[0].kind = "context";    // string copied
        // Note on kind: The kind field is used to indicate the type
        // of the object. This is to avoid conventions such as that used
        // by files (name.type -- e.g. test.ps = postscript etc.)
        CosNaming::NamingContext_var testContext;
        try {
            // Bind the context to root.
            testContext = rootContext->bind_new_context(contextName);
        }
        catch (const CosNaming::NamingContext::AlreadyBound&) {
            // If the context already exists, this exception will be raised.
            // In this case, just resolve the name and assign testContext
            // to the object returned:
            CORBA::Object_var obj = rootContext->resolve(contextName);
            testContext = CosNaming::NamingContext::_narrow(obj);
            if (CORBA::is_nil(testContext)) {
                std::cerr << "Failed to narrow naming context." << std::endl;
                return false;
            }
        }
        // Bind objref with name Bug to the my_context:
        CosNaming::Name objectName;
        objectName.length(1);
        objectName[0].id   = "Bug";
        objectName[0].kind = "Object";
        try {
            testContext->bind(objectName, objref);
        }
        catch (const CosNaming::NamingContext::AlreadyBound&) {
            testContext->rebind(objectName, objref);
        }
        // Note: Using rebind() will overwrite any Object previously bound
        // to /test/Echo with obj.
        // Alternatively, bind() can be used, which will raise a
        // CosNaming::NamingContext::AlreadyBound exception if the name
        // supplied is already bound to an object.
    }
    catch (const CORBA::TRANSIENT&) {
        std::cerr << "Caught system exception TRANSIENT -- unable to contact the "
                     "naming service." << std::endl
                  << "Make sure the naming server is running and that omniORB is "
                     "configured correctly." << std::endl;
        return false;
    }
    catch (const CORBA::SystemException &e) {
        std::cerr << "Caught a CORBA::" << e._name()
                  << " while using the naming service." << std::endl;
        return false;
    }
    return true;
}
