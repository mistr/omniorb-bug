.PHONY: all clean omni_start omni_stop omni_restart server_start server_stop server_restart client_start run \
        omni_daemonize server_daemonize

all: run

SERVER_BINARY = server
CLIENT_BINARY = client
CC_SERVER     = server.cc
CC_CLIENT     = client.cc
IDL_FILE      = Bug.idl
SERVER_LOG    = server_cpp.log
CLIENT_LOG    = client_to_cpp.log
OMNI_DIR      = omniNames/
OMNI_LOG     := $(OMNI_DIR)omniNames.log

OMNI_PORT    = 11111
SERVER_PORT := $(shell expr $(OMNI_PORT) + 1)

ALL_BINARY := $(SERVER_BINARY) $(CLIENT_BINARY)

OMNI_NAMES = omniNames
OMNI_IDL   = omniidl

SERVER_ARG := -ORBtraceLevel 40 -ORBnativeCharCodeSet UTF-8 -ORBendPoint giop:tcp::$(SERVER_PORT)
CLIENT_ARG = -ORBtraceLevel 40

HH_FILE := $(addsuffix .hh,$(basename $(IDL_FILE)))
CC_FILES := $(basename $(IDL_FILE))SK.cc $(basename $(IDL_FILE))DynSK.cc
O_FILES := $(CC_FILES:%.cc=%.o)
O_SERVER := $(CC_SERVER:%.cc=%.o)
O_CLIENT := $(CC_CLIENT:%.cc=%.o)

DBG_OPT = -ggdb3

$(SERVER_BINARY): $(O_SERVER)
$(CLIENT_BINARY): $(O_CLIENT)

$(ALL_BINARY): $(O_FILES)
	$(CXX) $(DBG_OPT) $^ -o $@ -lomniDynamic4 -lomniORB4 -lomnithread

$(O_SERVER) $(O_CLIENT): CXX_DEFS:=-DOMNI_PORT=\"$(OMNI_PORT)\"

$(O_SERVER) $(O_CLIENT) $(O_FILES): $(HH_FILE)

%.o: %.cc
	$(CXX) -W -Wall $(DBG_OPT) -I. $(CXX_DEFS) -o $@ -c $(filter %.cc,$^)

$(CC_FILES) $(HH_FILE): $(IDL_FILE)
	$(OMNI_IDL) -bcxx -Wbuse_quotes -Wba $<
	$(OMNI_IDL) -bpython $<

clean: omni_stop
	rm -f $(O_SERVER) $(O_CLIENT) $(O_FILES) $(CC_FILES) $(ALL_BINARY) $(HH_FILE) *.log
	rm -rf Bug Bug__POA Bug_idl.py Bug_idl.pyc
	rm -rf $(OMNI_DIR)

$(OMNI_DIR):
	mkdir -p $(OMNI_DIR)

omni_start: $(SERVER_BINARY) $(OMNI_DIR)
	@if ! fuser $(OMNI_LOG) >/dev/null 2>&1; then $(MAKE) omni_daemonize; fi

omni_daemonize:
	@rm -f $(OMNI_DIR)* $(OMNI_LOG)
	nohup $(OMNI_NAMES) -start $(OMNI_PORT) -logdir $(OMNI_DIR) -errlog $(OMNI_LOG) >/dev/null 2>&1 &
	@sleep 0.1
	@fuser $(OMNI_LOG) >/dev/null 2>&1 && echo "omniNames started"

omni_stop: server_stop
	@if fuser $(OMNI_LOG) >/dev/null 2>&1; then \
         fuser -sk $(OMNI_LOG) && sleep 0.1;\
         fuser $(OMNI_LOG) >/dev/null 2>&1 || echo "omniNames stopped";\
     fi

omni_restart: omni_stop
	$(MAKE) omni_start

server_start: omni_start
	@if ! fuser $(SERVER_BINARY) 2>&1 | grep -q "[0-9]\+e$$"; then $(MAKE) server_daemonize; fi

server_daemonize:
	nohup ./$(SERVER_BINARY) $(SERVER_ARG) >$(SERVER_LOG) 2>&1 &
	@sleep 0.1
	@fuser $(SERVER_BINARY) 2>&1 | grep -q "[0-9]\+e$$" && echo "C++ server started"

server_stop:
	@if fuser $(SERVER_BINARY) 2>&1 | grep -q "[0-9]\+e$$"; then \
         fuser -sk $(SERVER_BINARY);\
         sleep 0.1;\
         fuser $(SERVER_BINARY) 2>&1 | grep -q "[0-9]\+e$$" || echo "C++ server stopped";\
     fi

server_restart: server_stop
	$(MAKE) server_start

client_start: server_start
	./$(CLIENT_BINARY) $(CLIENT_ARG) >$(CLIENT_LOG) 2>&1

run: server_restart $(CLIENT_BINARY)
	$(MAKE) client_start &
	@sleep 0.3
	@fuser $(CLIENT_BINARY) >/dev/null 2>&1 && fuser -sk $(CLIENT_BINARY) && sleep 0.1 || \
     echo "client finished successfully"
	$(MAKE) server_stop
	nohup ./server.py $(SERVER_ARG) >server_py.log 2>&1 &
	@sleep 0.3
	./$(CLIENT_BINARY) $(CLIENT_ARG) >client_to_py.log 2>&1
	@fuser -sk server_py.log && echo "Python server finished successfully" || echo "Python server crashed"
