#!/usr/bin/env python
# vim: set fileencoding=utf-8 :

import sys
from omniORB import CORBA, PortableServer
import CosNaming, Bug, Bug__POA

# Define an implementation of the BugDemo interface
class BugDemo_i(Bug__POA.Demo):

    def call(self, value):
        if value is None:
            raise Bug.Demo.WRONG_DATA(value = None)
        if value == Bug.MISSING_VALUE:
            raise Bug.Demo.WRONG_DATA(value = None)
        if value == Bug.BAD_VALUE:
            raise Bug.Demo.WRONG_DATA(value = Bug.BAD_VALUE)
        if value == Bug.CORRECT_VALUE:
            return
        raise Bug.Demo.WRONG_DATA(value = Bug.BAD_VALUE)


if __name__ == "__main__":

    # Initialise the ORB and find the root POA
    orb = CORBA.ORB_init(sys.argv, CORBA.ORB_ID)
    poa = orb.resolve_initial_references("RootPOA")
    # Create an instance of Echo_i and an Echo object reference
    di = BugDemo_i()
    do = di._this()
    # Obtain a reference to the root naming context
    obj = orb.string_to_object("corbaname::localhost:11111")
    
    rootContext = obj._narrow(CosNaming.NamingContext)
    if rootContext is None:
        print "Failed to narrow the root naming context"
        sys.exit(1)
    # Bind a context named "context.my_context" to the root context
    name = [CosNaming.NameComponent("my_context", "context")]
    try:
        bugContext = rootContext.bind_new_context(name)
        print "New bug context bound"
    except CosNaming.NamingContext.AlreadyBound, ex:
        print "Bug context already exists"
        obj = rootContext.resolve(name)
        bugContext = obj._narrow(CosNaming.NamingContext)
        if bugContext is None:
            print "context.my_context exists but is not a NamingContext"
            sys.exit(1)
    # Bind the Bug object to the my_context
    name = [CosNaming.NameComponent("Bug", "Object")]
    try:
        bugContext.bind(name, do)
        print "New BugDemo object bound"
    except CosNaming.NamingContext.AlreadyBound:
        bugContext.rebind(name, do)
        print "BugDemo binding already existed -- rebound"
    # Activate the POA
    poaManager = poa._get_the_POAManager()
    poaManager.activate()
    print "server is running"
    # Block for ever (or until the ORB is shut down)
    orb.run()
    sys.exit(0)
