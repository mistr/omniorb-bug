#include "Bug.hh"
#include <cstdlib>
#include <iostream>
#include <sstream>

#ifndef OMNI_PORT
#define OMNI_PORT="11111"
#endif

CORBA::Object_ptr getObjectReference(CORBA::ORB_ptr orb);
void call(Bug::Demo_ptr s_ptr, Bug::OptionalValue *value);

//////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
    try {
        CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);
        CORBA::Object_var obj = getObjectReference(orb);
        Bug::Demo_var server = Bug::Demo::_narrow(obj);
        static const Bug::Value correct_value = Bug::CORRECT_VALUE;
        static const Bug::Value missing_value = Bug::MISSING_VALUE;
        static const Bug::Value bad_value     = Bug::BAD_VALUE;
        static const Bug::Value* const test_data[] = { &missing_value,
                                                       &correct_value,
                                                       &bad_value,
                                                       NULL,
                                                       &missing_value,
                                                       &correct_value,
                                                       &bad_value,
                                                       NULL,
                                                       &bad_value,
                                                       &bad_value,
                                                       &bad_value };
        static const Bug::Value* const* const test_data_end = test_data + (sizeof(test_data) / sizeof(*test_data));
        Bug::OptionalValue_var src;
        for (const Bug::Value* const* data = test_data; data < test_data_end; ++data) {
            if (*data == NULL) {
                src = NULL;
            }
            else {
                src = new Bug::OptionalValue(**data);
            }
            call(server, src.in());
        }
        orb->destroy();
    }
    catch (const CORBA::TRANSIENT&) {
        std::cerr << "Caught system exception TRANSIENT -- unable to contact the server." << std::endl;
        return EXIT_FAILURE;
    }
    catch (const CORBA::SystemException &e) {
        std::cerr << "Caught a CORBA::" << e._name() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const CORBA::Exception &e) {
        std::cerr << "Caught CORBA::Exception: " << e._name() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
//////////////////////////////////////////////////////////////////////

CORBA::Object_ptr getObjectReference(CORBA::ORB_ptr orb)
{
    CosNaming::NamingContext_var rootContext;
    try {
        // Obtain a reference to the root context of the Name service:
        //CORBA::Object_var obj = orb->resolve_initial_references("NameService");
        CORBA::Object_var obj = orb->string_to_object("corbaname::localhost:" OMNI_PORT);
        // Narrow the reference returned.
        rootContext = CosNaming::NamingContext::_narrow(obj);
        if (CORBA::is_nil(rootContext)) {
            std::cerr << "Failed to narrow the root naming context." << std::endl;
            return CORBA::Object::_nil();
        }
    }
    catch (const CORBA::NO_RESOURCES&) {
        std::cerr << "Caught NO_RESOURCES exception. You must configure omniORB with the location" << std::endl
                  << "of the naming service." << std::endl;
        return CORBA::Object::_nil();
    }
    catch (const CORBA::ORB::InvalidName&) {
        // This should not happen!
        std::cerr << "Service required is invalid [does not exist]." << std::endl;
        return CORBA::Object::_nil();
    }
    // Create a name object, containing the name test/context:
    CosNaming::Name name;
    name.length(2);
    name[0].id   = "my_context";
    name[0].kind = "context";
    name[1].id   = "Bug";
    name[1].kind = "Object";
    // Note on kind: The kind field is used to indicate the type
    // of the object. This is to avoid conventions such as that used
    // by files (name.type -- e.g. test.ps = postscript etc.)
    try {
        // Resolve the name to an object reference.
        return rootContext->resolve(name);
    }
    catch (const CosNaming::NamingContext::NotFound&) {
        // This exception is thrown if any of the components of the
        // path [contexts or the object] aren’t found:
        std::cerr << "Context not found." << std::endl;
    }
    catch (const CORBA::TRANSIENT&) {
        std::cerr << "Caught system exception TRANSIENT -- unable to contact the naming service." << std::endl
                  << "Make sure the naming server is running and that omniORB is configured correctly." << std::endl;
    }
    catch (const CORBA::SystemException &e) {
        std::cerr << "Caught a CORBA::" << e._name() << " while using the naming service." << std::endl;
    }
    return CORBA::Object::_nil();
}

void call(Bug::Demo_ptr s_ptr, Bug::OptionalValue *value)
{
    if (CORBA::is_nil(s_ptr)) {
        std::cerr << "test: The object reference is nil!" << std::endl;
        return;
    }
    std::cout << "\n============ ";
    if (value == NULL) {
        std::cout << "call(NULL)";
    }
    else {
        switch (value->_boxed_in()) {
            case Bug::BAD_VALUE:
                std::cout << "call(Bug::BAD_VALUE)";
                break;
            case Bug::MISSING_VALUE:
                std::cout << "call(Bug::MISSING_VALUE)";
                break;
            case Bug::CORRECT_VALUE:
                std::cout << "call(Bug::CORRECT_VALUE)";
                break;
            default:
                std::cout << "call(?)";
        }
    }
    std::cout << " ============" << std::endl;
    try {
        s_ptr->call(value);
        std::cout << "============ no exception ============" << std::endl;
    }
    catch (const Bug::Demo::WRONG_DATA &e) {
        std::cout << "============ ";
        if (e.value.in() == NULL) {
            std::cout << "throw Bug::Demo::WRONG_DATA(NULL)";
        }
        else {
            switch (e.value.in()->_boxed_in()) {
                case Bug::BAD_VALUE:
                    std::cout << "throw Bug::Demo::WRONG_DATA(Bug::BAD_VALUE)";
                    break;
                case Bug::MISSING_VALUE:
                    std::cout << "throw Bug::Demo::WRONG_DATA(Bug::MISSING_VALUE)";
                    break;
                case Bug::CORRECT_VALUE:
                    std::cout << "throw Bug::Demo::WRONG_DATA(Bug::CORRECT_VALUE)";
                    break;
                default:
                    std::cout << "throw Bug::Demo::WRONG_DATA(?)";
            }
        }
        std::cout << " ============" << std::endl;
    }
    catch (const CORBA::TRANSIENT&) {
        std::cerr << "============ catch: CORBA::TRANSIENT ============" << std::endl;
    }
    catch (const CORBA::SystemException &e) {
        std::cerr << "============ catch: CORBA::" << e._name() << " ============" << std::endl;
    }
    catch (const CORBA::Exception &e) {
        std::cerr << "============ catch: CORBA::Exception: " << e._name() << " ============" << std::endl;
    }
    catch (const std::exception &e) {
        std::cerr << "============ catch: unexpected exception " << e.what() << " ============" << std::endl;
    }
    catch (...) {
        std::cerr << "============ catch: unknown exception ============" << std::endl;
    }
}
